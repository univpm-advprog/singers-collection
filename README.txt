Tutorial MAVEN:

https://books.sonatype.com/mvnex-book/pdf/mvnex-pdf.pdf

Workflow:

1. configura utenti in /opt/tomcat
2. definisci server in ~/.m2/settings.xml
3. configure pom.xml / mvn

mvn dependency:list

mvn dependency:tree

mvn
    clean:clean *
    process-resources
    compile *
    process-test-resources
    test-compile
    test *
    package *
    install
    deploy * (tomcat7:deploy/undeploy/redeploy)

    site 

mvn tomcat7:deploy -Dmaven.test.skip=true
