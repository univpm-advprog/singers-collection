package it.univpm.advprog.singers.test.unit;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestExample {

	@BeforeAll
	public static void setup() {
		System.out.println("Setup (BeforeAll) ...");
	}
	
	@AfterAll
	public static void teardown() {
		System.out.println("Teardown (AfterAll) ...");
	}
	
	@BeforeEach
	public void prepareTest() {
		System.out.println("Prepare next test (BeforeEach) ...");
	}
	
	@AfterEach
	public void cleanupTest() {
		System.out.println("Cleanup last test (AfterEach) ...");
	}
	
	@Test
	public void thisIsTheFirstTest() {
		System.out.println("This is the first test");
		
		assertTrue(true);
	}
	
	@Test
	public void thisIsTheSecondTest() {
		System.out.println("This is the second test");
		
		assertTrue(true);
	}
	
	@Test
	public void thisIsTheThirdTest() {
		System.out.println("This is the third test");
		
		assertTrue(true);
	}
}
