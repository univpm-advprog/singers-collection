package it.univpm.advprog.singers.test.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import it.univpm.advprog.singers.model.dao.InstrumentDao;
import it.univpm.advprog.singers.model.entities.Instrument;
import it.univpm.advprog.singers.test.DataServiceConfigTest;

public class TestInstrumentDao {

	private static AnnotationConfigApplicationContext ctx;
	
	@BeforeAll
	static void setup() {
		System.out.println("Prepare the test environment");
		// TODO prepare data structure and storage, if needed
		
		ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class);
	}
	
	@AfterAll
	static void tearDown() {
		System.out.println("Clean-up the test environment");
		// TODO cleanup data structures and storage, if needed
		
		ctx.close();;
	}

	@Test
	void noInstrumentsAtBeginning() {
		//try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			InstrumentDao dao = ctx.getBean("instrumentDao", InstrumentDao.class);
			
			Session s  = sf.openSession();
			
			dao.setSession(s);
			
			assertEquals(dao.findAll().size(), 0);
		//}
	}
	
	@Test
	void createAndFind() {
		//try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			InstrumentDao dao = ctx.getBean("instrumentDao", InstrumentDao.class);
			
			Session s  = sf.openSession();
			
			s.beginTransaction();
			
			dao.setSession(s);
			
			Instrument i = dao.create("instrument1", "guitar cool", "guitar");
			
			try {
				dao.findById(i.getInstrumentId());
			} catch (Exception e) {
				fail("Exception not expected: " + e.getMessage());
			}
			
			try {
				Instrument notFound = dao.findById("foo");
				assertEquals(notFound, null); // exception expected because wrong id is searched
			} catch (Exception e) {
				assertTrue(true);
			}
			
			s.getTransaction().commit();
			
			List<Instrument> allInstruments = dao.findAll();
			assertEquals(allInstruments.size(), 1);
		//}
	}
	

	@Test
	void createAndUpdate() {
		//try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			InstrumentDao dao = ctx.getBean("instrumentDao", InstrumentDao.class);
			
			Session s  = sf.openSession();
			
			dao.setSession(s);
			
			s.beginTransaction();
			
			Instrument i1 = dao.create("instrument1", "guitar cool", "guitar");
			
			s.getTransaction().commit();
			
			s.beginTransaction();
			assertEquals(dao.findAll().size(), 1);

			Instrument i2 = new Instrument();
			i2.setInstrumentId(i1.getInstrumentId());
			i2.setName("newName");
			i2.setFamily(i1.getFamily());
			
			assertEquals(i1.getName(), "guitar cool");
			assertNotEquals(i1.getName(), "newName");
			
			Instrument i3 = dao.update(i2);
			s.getTransaction().commit();
			
			s.beginTransaction();
			// i2 is detached, i3 is attached
			assertNotEquals(i2, i3);
			
			// i1 and i2 are attached, and have the same id
			assertEquals(i3, i1);		
			
			// 
			assertNotEquals(i1.getName(), "guitar cool");
			assertEquals(i1.getName(), "newName");
			
			// only one instrument is created and updated
			assertEquals(dao.findAll().size(), 1);
			
			s.getTransaction().commit();
		//}
		//catch (Exception e) {
		//	fail("Unexpected error: " + e);
		//}
	}
	
}
