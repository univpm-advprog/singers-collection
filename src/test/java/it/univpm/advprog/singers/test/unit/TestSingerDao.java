package it.univpm.advprog.singers.test.unit;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import it.univpm.advprog.singers.model.dao.SingerDao;
import it.univpm.advprog.singers.model.dao.SingerDaoDefault;
import it.univpm.advprog.singers.model.entities.Singer;
import it.univpm.advprog.singers.test.DataServiceConfigTest;

public class TestSingerDao {

	private AnnotationConfigApplicationContext ctx;
	private SingerDao singerDao;
	private SessionFactory sf;

	@BeforeAll
	static void setup() {
		System.out.println("Prepare the test suite environment");
		// TODO prepare data structure and storage, if needed

	}

	@AfterAll
	static void tearDown() {
		System.out.println("Clean-up the test suite environment");
		// TODO cleanup data structures and storage, if needed

	}
	
	@BeforeEach
	void openContext() {
		/**
		 * Ciascun test ha bisogno di un contesto applicativo e un singerDao
		 */
		System.out.println("Prepare the test case environment");

		ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class);
		
		singerDao = ctx.getBean("singerDao", SingerDao.class);
		
		sf = ctx.getBean("sessionFactory", SessionFactory.class);
	}
	
	@AfterEach
	void closeContext() {
		System.out.println("Clean-up the test case environment");
		
		ctx.close();
	}

	@Test
	void testBeginCommitTransaction() {
		/**
		 * Check that the dao stores the session for later use, allowing to share the
		 * session
		 */

		Session s = sf.openSession();

		assertTrue(s.isOpen());

		s.beginTransaction();

		singerDao.setSession(s);

		assertEquals(s, singerDao.getSession()); // s.equals(singerDao.getSession());
		assertSame(s, singerDao.getSession()); // s == singerDao.getSession();
		assertTrue(singerDao.getSession().getTransaction().isActive());

		s.getTransaction().commit();

		assertFalse(singerDao.getSession().getTransaction().isActive());

	}

	@Test
	void testAutoCreationOfSession() {
		/**
		 * Check that the dao opens a new session if no session is passed
		 * (NB the returned session has transaction disabled by default)
		 * 		 
		 */
		
		Session s = singerDao.getSession();
		assertNotNull(s); // s.equals(singerDao.getSession());
		assertFalse(s.getTransaction().isActive());
	}



	@Test
	void testCreateSingerDuplicateNames() {
		/**
		 * We test that it is possible to create two singers with same name and surname
		 */
		
		Session s = sf.openSession();

		singerDao.setSession(s);

		Singer newSinger1 = singerDao.create("nome", "cognome", null);

		try {
			Singer newSinger2 = singerDao.create(newSinger1.getFirstName(), newSinger1.getLastName(), null);
			assertTrue(true);
		} catch (Exception e) {
			// pass
			fail("Unexpected exception creating singer with duplicate name: " + e.getMessage());
		}

	}

	@Test
	void testNoSingersAtBeginning() {
		/**
		 * Check that there are no singers when the application loads
		 */

		Session s = sf.openSession();

		singerDao.setSession(s);

		List<Singer> allSingers = singerDao.findAll();

		assertEquals(allSingers.size(), 0);
	}

	@Test
	void testAllCreatedAreFound() {
		/**
		 * Generate N singers, find all of them
		 */
		int N = 10;

		Session s = sf.openSession();

		singerDao.setSession(s);

		for (int i = 0; i < N; i++) {
			singerDao.create("Nome" + i, "Cognome" + i, null);

			List<Singer> allSingers = singerDao.findAll();
			assertEquals(allSingers.size(), i + 1);
		}
	}
	
	@Test
	void testSingerCanHaveNoFirstName() {
		/**
		 * A singer can have empty first name, provided that it has non empty last name
		 */
		Session s = sf.openSession();

		Singer newSinger = singerDao.create("", "cognome", null);

		assertNotNull(newSinger);
	}
	
	@Test
	void testSingerCanHaveNoLastName() {
		/**
		 * A singer can have empty last name, provided that it has non empty first name
		 */
	
		Session s = sf.openSession();

		Singer newSinger = singerDao.create("nome", "", null);

		assertNotNull(newSinger);
	}

	@Test
	void testSingerMustHaveAName() {
		/**
		 * A singer cannot have both empty first name and empty last name 
		 * 
		 * NB now it is failing
		 */
		Session s = sf.openSession();

		// assertThrows(singerDao.create("", "", null)); // NB this is not working
		
		// assertThrows(Exception.class, singerDao.create("", "", null) ); // NB this is not working either

		/*
		 * This is working but it's tedious to write
		 * 
		 * try {
		 * 
		 * 		singerDao.create("", "", null);
		 * 		fail("Exception expected when creating singer with both empty first name and empty last name");
		 * } catch (Exception e) {
		 * 		assertTrue(true);
		 * }
		 */
		
		
		assertThrows(Exception.class, () ->  { singerDao.create("", "", null); } );
	}
	
	@Test
	void testSingerSlowTask() {
		/**
		 * A singer cannot have both empty first name and empty last name 
		 * 
		 * NB now it is failing
		 */		
		assertTrue(singerDao instanceof SingerDaoDefault);
		
		Session s = sf.openSession();

		/*
		 * This is working but it's tedious to write
		 * 
		 * long timeout = 100, begin, end;
		 * 
		 * begin = now();
		 * ((SingerDaoDefault) singerDao).slowTask(1010);
		 * end = now();
		 * 
		 * if (end - begin > timeout) {	
		 * 		fail("Timeout executing expression");
		 * } else
		 * 		assertTrue(true);
		 * }
		 */

		
		assertTimeout(Duration.ofMillis(1000), () ->  { ((SingerDaoDefault) singerDao).slowTask(100); } );
	}
	
	@Test
	void testAllCreatedAreFoundById() {
		/**
		 * Generate N singers, find all of them by ID, check the found singer is the same instance that was 
		 * returned by the SingerDao.create(...) method
		 */
		int N = 10;

		Session s = sf.openSession();

		singerDao.setSession(s);

		for (int i = 0; i < N; i++) {
			Singer inserted = singerDao.create("Nome" + i, "Cognome" + i, null);

			Singer found = singerDao.findById(inserted.getId());
			
			assertSame(inserted, found);
		}
	}
	
	@Test
	void testSingerNotFoundById() {
		Session s = sf.openSession();

		singerDao.setSession(s);

		Singer inserted = singerDao.create("Nome", "Cognome", null);
		
		Singer found = singerDao.findById(inserted.getId() + 10);
		
		assertNull(found);
	}
	
	@Test
	void testSingerIsUpdatedCorrectlyWithMerging() {
		Session s = sf.openSession();

		singerDao.setSession(s);

		Singer inserted = singerDao.create("Nome", "Cognome", null);
		
		Singer updated = new Singer();
		updated.setId(inserted.getId());
		updated.setFirstName("Nome1");
		updated.setLastName("Cognome1");
		
		updated = singerDao.update(updated);
		
		Singer found = singerDao.findById(inserted.getId());
		
		assertSame(inserted, updated);
		assertSame(updated, found);
		assertSame(found, inserted);
	}

	
	@Test
	void testSingerIsUpdatedCorrectlyWithoutMerging() {
		Session s = sf.openSession();

		singerDao.setSession(s);

		Singer inserted = singerDao.create("Nome", "Cognome", null);
		
		Singer updated = new Singer();
		updated.setId(inserted.getId());
		updated.setFirstName("Nome1");
		updated.setLastName("Cognome1");
		
		singerDao.update(updated);
		
		Singer found = singerDao.findById(inserted.getId());
		
		assertNotNull(found);
		assertEquals(found, inserted);
		assertSame(found, inserted);
		
		//assertEquals(found, updated);
		assertNotSame(found, updated);
		
		assertEquals(found.getFirstName(), updated.getFirstName());
		assertEquals(found.getLastName(), updated.getLastName());
		assertEquals(found.getId(), updated.getId());
		assertEquals(found.getBirthDate(), updated.getBirthDate());
		
	}

	@Test
	void testSingerIsCreatedAndDeleted() {
		Session s = sf.openSession();

		singerDao.setSession(s);
		
		// 1. create a singer
		s.beginTransaction();

		assertEquals(0, singerDao.findAll().size());
		
		Singer inserted = singerDao.create("Nome", "Cognome", null);
		
		s.getTransaction().commit();
		
		// 2. delete the singer
		s.beginTransaction();
		
		assertEquals(1, singerDao.findAll().size());
		
		singerDao.delete(inserted);
		
		s.getTransaction().commit();
		
		// 3. check no more singers
		assertEquals(0, singerDao.findAll().size());
		
	}
	
	@Test
	void testDeleteNonExistingSingerDoesNotCauseError() {
		/**
		 * A singer that does not exist can be deleted without begin noticed to the callee
		 * 
		 */
		Session s = sf.openSession();

		singerDao.setSession(s);
				
		Singer fake = new Singer();
		fake.setId(100L);
		
		assertNull(singerDao.findById(fake.getId()));
		
		try {
			singerDao.delete(fake);
			assertTrue(true);
		} catch (Exception e) {
			fail("Unexpected exception when deleting fake singer");
		}
		
	}
	
	@Test
	void testJustCreatedSingerHasNoAlbums() {
		Session s = sf.openSession();

		singerDao.setSession(s);
		
		Singer inserted = singerDao.create("Nome", "Cognome", null);
		
		assertEquals(0, inserted.getAlbums().size());

	}


}
