package it.univpm.advprog.singers.model.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import it.univpm.advprog.singers.utils.LocalDateAttributeConverter;

@Entity
@Table(name = "singer")
@NamedQueries({
    @NamedQuery(name="Singer.findAllWithAlbum",
     query="select distinct s from Singer s " +
        "left join fetch s.albums a " +
        "left join fetch s.instruments i")
})
public class Singer implements Serializable {

	private static final long serialVersionUID = 1847253395173328425L;

	private Long id;
	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	private int version;
	
	private Set<Album> albums = new HashSet<Album>();
	private Set<Instrument> instruments = new HashSet<Instrument>();

	public void setId(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	public Long getId() {
		return this.id;
	}

	@Version
	@Column(name = "VERSION")
	public int getVersion() {
		return version;
	}

	@Column(name = "FIRST_NAME")
	public String getFirstName() {
		return this.firstName;
	}

	@Column(name = "LAST_NAME")
	public String getLastName() {
		return this.lastName;
	}

	@Column(name = "BIRTH_DATE")
	@Convert(converter = LocalDateAttributeConverter.class)
	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String toString() {
		return "Singer - Id: " + id + ", First name: " + firstName + ", Last name: " + lastName + ", Birthday: "
				+ birthDate;
	}

	/**
	 * Note that this property is derived from mapped fields, but it's not mapped itself in the db (thus the @Transient annotation)
	 * 
	 * @return The full name of the singer
	 */
	@Transient
	public String getFullName() {
		return (this.firstName + " " + this.lastName).trim();
	}
	
	@OneToMany(mappedBy="singer", cascade=CascadeType.ALL,
			orphanRemoval=true)
	public Set<Album> getAlbums() {
		return this.albums;
	}
	
	public void addAlbum(Album album) {
		album.setSinger(this);
		this.albums.add(album);
		
	}
	
	public void setAlbums(Set<Album> albums) {
		this.albums = albums;
	}
	
	@ManyToMany(fetch = FetchType.EAGER,        
			cascade =
	        {
	                CascadeType.DETACH,
	                CascadeType.MERGE,
	                CascadeType.REFRESH,
	                CascadeType.PERSIST
	        })
	@JoinTable(name="singer_instrument",
		 joinColumns = @JoinColumn(name = "SINGER_ID", nullable = false,
                 updatable = false), 
		 inverseJoinColumns = @JoinColumn(name = "INSTRUMENT_ID", nullable = false,
         updatable = false)) 
	public Set<Instrument> getInstruments() {
		return this.instruments;
	}
	
	public void addInstrument(Instrument i) {
		this.instruments.add(i);
		i.getSingers().add(this); // NB notare che non usiamo l'utility method addSinger
	}
	
	public void removeInstrument(Instrument i) {
		this.instruments.remove(i);
		i.getSingers().remove(this);
	}
	
	public void setInstruments(Set<Instrument> instruments) {
		this.instruments = instruments;
	}
	
}
