package it.univpm.advprog.singers.model.dao;

import java.util.List;

import org.hibernate.Session;

import it.univpm.advprog.singers.model.entities.Album;
import it.univpm.advprog.singers.model.entities.Singer;

public interface AlbumDao {
	Session getSession();
	public void setSession(Session session);

	
	List<Album> findAll();
	
	Album findById(Long id);

	Album create(String title);
	
	Album create(String title, Singer singer);
	
	Album update(Album album);
	
	void delete(Album album);


}
