package it.univpm.advprog.singers.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "instrument")
public class Instrument implements Serializable {
	
	private String instrumentId;
	private String name;
	private String family;
	private Set<Singer> singers = new HashSet<Singer>();

	@Id
	@Column(name = "INSTRUMENT_ID")
	public String getInstrumentId() {
		return this.instrumentId;
	}

	public void setInstrumentId(String instrumentId) {
		this.instrumentId = instrumentId;
	}

	@Column
	public String getFamily() {
		return this.family;
	}
	
	public void setFamily(String family) {
		this.family = family;
	}
	
	@Column
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Instrument : " + getInstrumentId();
	}
	
//	@ManyToMany(fetch = FetchType.EAGER,         cascade =
//		        {
//		                CascadeType.DETACH,
//		                CascadeType.MERGE,
//		                CascadeType.REFRESH,
//		                CascadeType.PERSIST
//		        })
//	@JoinTable(name="singer_instrument", 
//			joinColumns = @JoinColumn(name = "INSTRUMENT_ID", nullable = false,
//                 updatable = false),
//			inverseJoinColumns = @JoinColumn(name = "SINGER_ID" ,nullable = false,
//	                updatable = false))
	@ManyToMany(fetch = FetchType.EAGER,         cascade =
	        {
	                CascadeType.DETACH,
	                CascadeType.MERGE,
	                CascadeType.REFRESH,
	                CascadeType.PERSIST
	        }, mappedBy = "instruments")
	public Set<Singer> getSingers() {
		return this.singers;
	}
	
	public void addSinger(Singer s) {
		this.singers.add(s);
		s.getInstruments().add(this); // NB nota che non usiamo l'utility method addInstrument
	}
	
	public void removeSinger(Singer s) {
		this.singers.remove(s);
		s.getInstruments().remove(this);
	}
	
	public void setSingers(Set<Singer> singers) {
		this.singers = singers;
	}

}
