package it.univpm.advprog.singers.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.univpm.advprog.singers.model.entities.Album;
import it.univpm.advprog.singers.model.entities.Singer;

//@Transactional
@Repository("albumDao")
public class AlbumDaoDefault extends DefaultDao implements AlbumDao {
	
	@Override
//	@Transactional(readOnly = true)
	public List<Album> findAll() {
		return getSession().
			createQuery("from Album a", Album.class).
			getResultList();
	}

	@Override
//	@Transactional(readOnly = true)
	public Album findById(Long id) {
		return getSession().find(Album.class, id);
	}

	@Override
//	@Transactional
	public void delete(Album album) {
		this.getSession().delete(album);
	}

	@Override
//	@Transactional
	public Album create(String title) {
		return this.create(title, null);
	}

	@Override
//	@Transactional
	public Album create(String title, Singer singer) {
		Album album = new Album();
		album.setTitle(title);
		album.setSinger(singer);
		this.getSession().save(album);
		return album;
	}

	@Override
//	@Transactional
	public Album update(Album album) {
		return (Album)this.getSession().merge(album);
	}

}
