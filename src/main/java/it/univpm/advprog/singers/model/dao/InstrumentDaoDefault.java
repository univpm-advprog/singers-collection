package it.univpm.advprog.singers.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.univpm.advprog.singers.model.entities.Instrument;

//@Transactional
@Repository("instrumentDao")
public class InstrumentDaoDefault extends DefaultDao implements InstrumentDao  {

	@Override
	public List<Instrument> findAll() {
		return this.getSession().createQuery("FROM Instrument i", Instrument.class).getResultList();
	}

	@Override
	public Instrument findById(String id) {
		return this.getSession().find(Instrument.class, id);
	}

	@Override
	public Instrument update(Instrument instrument) {
		return (Instrument)this.getSession().merge(instrument);
	}

	@Override
	public void delete(Instrument instrument) {
		this.getSession().delete(instrument);
	
	}

	@Override
	public Instrument create(String id, String name, String family) {
		Instrument i = new Instrument();
		i.setInstrumentId(id);
		i.setName(name);
		i.setFamily(family);
		this.getSession().save(i);
		return i;
	}

	
	@Override
	public Instrument findByName(String name) {
		return this.getSession().createQuery("FROM Instrument i WHERE i.name = :name", Instrument.class).setParameter("name", name).getSingleResult();
	}
}
