package it.univpm.advprog.singers.model.dao;

import org.hibernate.Session;
import org.springframework.security.crypto.password.PasswordEncoder;

import it.univpm.advprog.singers.model.entities.User;


public interface UserDetailsDao {
	Session getSession();
	public void setSession(Session session);

	
	User findUserByUsername(String username);
	
	User create(String username, String password, boolean isEnabled);
	
	User update(User user);
	
	void delete(User user);

	public String encryptPassword(String password);
	
	void setPasswordEncoder(PasswordEncoder passwordEncoder);
	
	PasswordEncoder getpasswordEncoder();
}
