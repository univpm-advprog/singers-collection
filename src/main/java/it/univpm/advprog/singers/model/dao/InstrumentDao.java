package it.univpm.advprog.singers.model.dao;

import java.util.List;

import org.hibernate.Session;

import it.univpm.advprog.singers.model.entities.Instrument;

public interface InstrumentDao {
	
	Session getSession();
	public void setSession(Session session);

	Instrument findByName(String name);
	
	List<Instrument> findAll();

	Instrument findById(String id);

	Instrument create(String id, String name, String family);

	Instrument update(Instrument instrument);

	void delete(Instrument contact);
	
}
