package it.univpm.advprog.singers.model.dao;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import it.univpm.advprog.singers.model.entities.Album;
import it.univpm.advprog.singers.model.entities.Singer;

//@Transactional
@Repository("singerDao")
public class SingerDaoDefault extends DefaultDao implements SingerDao {
	
	//@Autowired
	//InstrumentDao instrumentDao;
	
	public List<Singer> findAll() {
		return getSession().
				createQuery("from Singer s", Singer.class).
				getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Singer> findAllWithAlbum() {
		// TODO osservare del compilatore per casting implicito
		return getSession().
				getNamedQuery("Singer.findAllWithAlbum").getResultList(); 
	}

	public Singer findById(Long id) {
		return getSession().find(Singer.class, id);
	}
	
	@Override
	public Singer update(Singer singer) {
		Singer merged = (Singer)this.getSession().merge(singer);
		return merged;
	}
	
	/**
	 * If the passed singer exists, it gets deleted from the context. If it does not exist, nothing happens
	 * (and no error is raised, either)
	 */
	public void delete(Singer singer) {
		this.getSession().delete(singer);

	}

	@Override
	public Singer create(String firstName, String lastName, LocalDate birthDate) {
		
		if ((firstName == null || firstName.length() == 0) && 
				(lastName == null || lastName.length() == 0)) {
			throw new RuntimeException("A singer must have a first name or a last name");
		}
		
		Singer s = new Singer();
		s.setFirstName(firstName);
		s.setLastName(lastName);
		s.setBirthDate(birthDate);
		
		this.getSession().save(s);
		
		return s;
	}

	@Override
	public Set<Album> getAlbums(Singer singer) {
		Query q = this.getSession().createQuery("from Album a JOIN FETCH a.singer WHERE a.singer = :singer", Album.class);
		
		return new HashSet<Album>(q.setParameter("singer", singer).getResultList());
	}
	
	public void slowTask(int wait) {
		/**
		 * This task is of no use, but we use for testing purposes.
		 * 
		 * int wait : number of milliseconds to wait
		 */
		try {
			System.out.println("Begin slow task");
			Thread.sleep(wait);
			System.out.println("End slow task");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
