package it.univpm.advprog.singers.model.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import it.univpm.advprog.singers.model.entities.Album;
import it.univpm.advprog.singers.model.entities.Singer;

public interface SingerDao {
	
	public Session getSession();
	public void setSession(Session session);
		
	List<Singer> findAll();

	List<Singer> findAllWithAlbum();
	
	Singer findById(Long id);

	Singer create(String string, String string2, LocalDate birthdate);
	
	Singer update(Singer singer);
	
	void delete(Singer contact);
	
	Set<Album> getAlbums(Singer singer);

}
