package it.univpm.advprog.singers.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.univpm.advprog.singers.model.entities.Instrument;
import it.univpm.advprog.singers.services.InstrumentService;

@RequestMapping("/instruments")
@Controller
public class InstrumentController {
	private final Logger logger = LoggerFactory.getLogger(InstrumentController.class);
	
	private InstrumentService instrumentService;
//	private MessageSource messageSource;


	// handle urls like this:
	// 
	// http://localhost/.../instruments/list
	// http://localhost/.../instruments/list?message=Errore%20parametri%20operazione
	//
	@GetMapping(value = "/list")
	public String list(@RequestParam(value = "message", required=false) String message, Model uiModel) {
		logger.info("Listing instruments");
		List<Instrument> allInstruments = this.instrumentService.findAll();
		
		uiModel.addAttribute("instruments", allInstruments);
		uiModel.addAttribute("numInstruments", allInstruments.size());
		
		// TODO ricevere un parametro via GET (es. per messaggio di esito operazione)
		uiModel.addAttribute("message", message);
		
		return "instruments/list";
	}
	
	@GetMapping(value = "/{instrumentId}/delete")
	public String delete(@PathVariable("instrumentId") String instrumentId) {
		this.instrumentService.delete(instrumentId);
		
		return "redirect:/instruments/list";
	}
	
	@GetMapping(value = "/add")
	public String add(Model uiModel) {
		
		uiModel.addAttribute("instrument", new Instrument());
		
		return "instruments/form";
	}

	@GetMapping(value="/{instrumentId}/edit")
	public String edit(@PathVariable("instrumentId") String instrumentId, 
			Model uiModel) {
		
		Instrument i = this.instrumentService.findById(instrumentId);
		uiModel.addAttribute("instrument", i);
		
		return "instruments/form";
	}

	private void sanitizeId(String instrumentId) {
		if (instrumentId.contains("/")) {
			throw new RuntimeException("Id strumento malformato");
		}
	}
	
	@PostMapping(value="/save")
	public String save(@ModelAttribute("instrument") Instrument instrument, 
			BindingResult br, Model uiModel) {
		
		try {
			this.sanitizeId(instrument.getInstrumentId());
			
			this.instrumentService.update(instrument);
			
			String strMessage = "Strumento (" + instrument.getInstrumentId() + "," + instrument.getFamily() + ") salvato correttamente";
			//uiModel.addAttribute("message", strMessage);
			
			return "redirect:/instruments/list?message=" + strMessage;
		} catch (RuntimeException e) {
			return "redirect:/instruments/list?message=" + e.getMessage();
		}
	}
	
	@Autowired
	public void setInstrumentService(InstrumentService instrumentService) {
		this.instrumentService = instrumentService;
	}
}
