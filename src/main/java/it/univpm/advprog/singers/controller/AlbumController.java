package it.univpm.advprog.singers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import it.univpm.advprog.singers.model.entities.Album;
import it.univpm.advprog.singers.services.AlbumService;

@RequestMapping("/albums")
@Controller
public class AlbumController {
	
	private AlbumService albumService;
	
	@GetMapping(value="/list")
	public String list(Model uiModel) {
		
		List<Album> albums = this.albumService.findAll();
		uiModel.addAttribute("albums", albums);
		
		return "albums/list";
	}
	
	@GetMapping(value="/add")
	public String add(Model uiModel) {
		
		uiModel.addAttribute("album", new Album());
		
		return "albums/form";
	}
	
	@PostMapping(value="/save")
	public String save(@ModelAttribute("album") Album album, BindingResult bindingResult) {
		
		this.albumService.update(album);
		
		return "redirect:/albums/list";
	}
	
	
	@Autowired
	public void setAlbumService(AlbumService albumService) {
		this.albumService = albumService;
	}
}
