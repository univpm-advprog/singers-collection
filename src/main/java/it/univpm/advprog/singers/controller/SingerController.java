package it.univpm.advprog.singers.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.univpm.advprog.singers.model.entities.Instrument;
import it.univpm.advprog.singers.model.entities.Singer;
import it.univpm.advprog.singers.services.InstrumentService;
import it.univpm.advprog.singers.services.SingerService;

@RequestMapping("/singers")
@Controller
public class SingerController {
	private final Logger logger = LoggerFactory.getLogger(SingerController.class);
	
	private SingerService singerService;
	private InstrumentService instrumentService;
//	private MessageSource messageSource;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(Model uiModel) {
		logger.info("Listing singers");
		
		List<Singer> allSingers = new ArrayList<Singer>();
		int numSingers = -1;
		
		try {
			allSingers = this.singerService.findAll();
			numSingers = allSingers.size();
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		uiModel.addAttribute("singers", allSingers);
		uiModel.addAttribute("numSingers", numSingers);
		
		return "singers/list";
		
	}
	
	@GetMapping(value = "/add")
	public String add(Model uiModel) {
		uiModel.addAttribute("newSinger", new Singer());
		return "singers/form";
		
	}
	
	@GetMapping(value = "/{singerId}/edit")
	public String edit(@PathVariable("singerId") String singerId, Model uiModel) {
		Singer s = this.singerService.findById(new Long(singerId));
		uiModel.addAttribute("newSinger", s);
		return "singers/form";
		
	}
	
	// NB @@PostMapping e` una scorciatoia per @RequestMapping(..., method=RequestMethod.POST)
	@PostMapping(value = "/save")
	public String save(@ModelAttribute("newSinger") Singer newSinger, BindingResult br) {
		this.singerService.update(newSinger);
		
		return "redirect:/singers/list/";
		
		// return "redirect:singers/list"; // NB questo non funzionerebbe!
		
	}
	

	@GetMapping(value = "/{singerId}/delete/")
	public String delete(@PathVariable("singerId") String singerId) {
		this.singerService.delete(new Long(singerId));
		return "redirect:/singers/list/";
	}


	// handle urls like:
	// http://localhost/.../singers/123/instrument/456/unlink?next=/singers/list
	//
	// NB @GetMapping e` una scorciatoia per @RequestMapping(..., method=RequestMethod.GET)
	@GetMapping(value = "/{singerId}/instrument/{instrumentId}/unlink/" )
	public String unlinkInstrument(
			@RequestParam(value = "next", required=false) String next, 
			@PathVariable("singerId") String singerId, 
			@PathVariable("instrumentId") String instrumentId)
	{
		this.singerService.unlinkInstrument(new Long(singerId), instrumentId);
		
		// NB: sotto e` molto importante la barra (/) prima di singers/list, perche` la struttura del redirect
		// dev'essere:
		// redirect:<URL>
		// e /singers/list e` appunta una URL della nostra applicazione. Nella maggior parte degli altri controller, 
		// invece, si ritornava 'singers/list' che era un *nome di vista*, non una URL (!!!)
		
		if (next == null || next.length() == 0) {
			next = "/singers/list";
		}
		
		return "redirect:" + next;
	}
	
	@GetMapping("/link/choose")
	public String link(Model uiModel) {
		uiModel.addAttribute("singers", this.singerService.findAll());
		uiModel.addAttribute("instruments", this.instrumentService.findAll());
		
		return "singers/link_choose";
	}
	
	// handle requests like this:
	//
	// http://localhost/.../link/?singer=123&instrument=456&next=/instruments/list
	//
	@PostMapping("/link")
	public String link(
			@RequestParam(value="next", required=false) String next,
			@RequestParam(value="instrument") String instrumentId,
			@RequestParam(value="singer") String singerId) {
		Instrument i = this.instrumentService.findById(instrumentId);
		Singer s = this.singerService.findById(new Long(singerId));
		
		
		s.getInstruments().add(i);
		i.getSingers().add(s);
		this.singerService.update(s);
		
		if (next == null || next.length() == 0) {
			next = "/singers/list";
		}
		
		return "redirect:" + next;
	}

	
	@Autowired
	public void setSingerService(SingerService singerService) {
		this.singerService = singerService;
	}
	
	@Autowired
	public void setInstrumentService(InstrumentService instrumentService) {
		this.instrumentService = instrumentService;
	}
}
