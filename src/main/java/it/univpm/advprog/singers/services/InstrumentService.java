package it.univpm.advprog.singers.services;

import java.util.List;

import it.univpm.advprog.singers.model.entities.Instrument;

public interface InstrumentService {
	List<Instrument> findAll();
	
	Instrument findById(String id);
	
	Instrument create(String id, String name, String family);
	
	Instrument update(Instrument instrument);
	
	void delete(String instrumentId);

}
