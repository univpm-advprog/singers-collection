package it.univpm.advprog.singers.services;

import java.time.LocalDate;
import java.util.List;

import it.univpm.advprog.singers.model.entities.Singer;

public interface SingerService {
	List<Singer> findAll();
	
	Singer findById(Long id);
	
	Singer create(String string, String string2);
	
	Singer create(String string, String string2, LocalDate birthdate);
	
	Singer update(Singer singer);
	
	void delete(Long long1);

	void delete(Singer contact);

	void unlinkInstrument(Long long1, String instrumentId);

}
