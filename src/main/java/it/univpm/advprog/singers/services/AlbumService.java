package it.univpm.advprog.singers.services;

import java.util.List;

import it.univpm.advprog.singers.model.entities.Album;
import it.univpm.advprog.singers.model.entities.Singer;

public interface AlbumService {

	public List<Album> findAll();
	
	public Album create(String name);
	
	public Album create(String title, Singer singer);
	
	public Album findById(Long id);
	
	public void delete(Album album);
	
	public Album update(Album album);

	
	
}
