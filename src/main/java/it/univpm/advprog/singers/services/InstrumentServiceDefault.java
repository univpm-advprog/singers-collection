package it.univpm.advprog.singers.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.univpm.advprog.singers.model.dao.InstrumentDao;
import it.univpm.advprog.singers.model.entities.Instrument;

@Transactional
@Service("instrumentService") // TODO provare a nominare questo servizio come singerService (es. per errore di copia-incolla) e vedi eccezione
public class InstrumentServiceDefault implements InstrumentService {
	
	InstrumentDao instrumentRepository;

	@Override
	public List<Instrument> findAll() {
		return this.instrumentRepository.findAll();
	}

	@Override
	public Instrument findById(String id) {
		return this.instrumentRepository.findById(id);
	}

	@Override
	public Instrument create(String id, String name, String family) {
		return this.instrumentRepository.create(id, name, family);
	}

	@Override
	public Instrument update(Instrument instrument) {
		return this.instrumentRepository.update(instrument);
	}

	@Override
	public void delete(String instrumentId) {
		Instrument instrument = this.instrumentRepository.findById(instrumentId);
		this.instrumentRepository.delete(instrument);
	}
	
	@Autowired
	public void setInstrumentRepository(InstrumentDao instrumentRepository) {
		this.instrumentRepository = instrumentRepository;
	}


}
