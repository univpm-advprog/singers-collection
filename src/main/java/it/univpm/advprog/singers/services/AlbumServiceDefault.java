package it.univpm.advprog.singers.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.univpm.advprog.singers.model.dao.AlbumDao;
import it.univpm.advprog.singers.model.entities.Album;
import it.univpm.advprog.singers.model.entities.Singer;

@Transactional
@Service("albumService")
public class AlbumServiceDefault implements AlbumService {

	private AlbumDao albumDao;
	
	@Override
	public List<Album> findAll() {
		return this.albumDao.findAll();
	}

	@Override
	public Album create(String title) {
		return this.albumDao.create(title);
	}
	
	@Override
	public Album create(String title, Singer singer) {
		return this.albumDao.create(title, singer);
	}

	@Override
	public Album findById(Long id) {
		return this.albumDao.findById(id);
	}

	@Override
	public void delete(Album album) {
		this.albumDao.delete(album);

	}

	@Override
	public Album update(Album album) {
		return this.albumDao.update(album);
	}

	@Autowired
	public void setAlbumDao(AlbumDao albumDao) {
		this.albumDao = albumDao;
	}
}
