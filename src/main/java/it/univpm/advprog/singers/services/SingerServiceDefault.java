package it.univpm.advprog.singers.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.univpm.advprog.singers.model.dao.InstrumentDao;
import it.univpm.advprog.singers.model.dao.SingerDao;
import it.univpm.advprog.singers.model.entities.Instrument;
import it.univpm.advprog.singers.model.entities.Singer;

@Transactional
@Service("singerService")
public class SingerServiceDefault implements SingerService {

	private SingerDao singerRepository;
	private InstrumentDao instrumentRepository;
	
	@Transactional(readOnly=true)
	@Override
	public Singer findById(Long id) {
		return this.singerRepository.findById(id);
	}

	@Transactional(readOnly=true)
	public List<Singer> findAll() {
		return this.singerRepository.findAll();
	}

	@Transactional
	@Override
	public Singer create(String firstName, String lastName) {
		return this.create(firstName, lastName, null);
	}

	@Transactional
	@Override
	public Singer create(String firstName, String lastName, LocalDate birthDate) {
		return this.singerRepository.create(firstName, lastName, birthDate);

	}
	
	@Transactional
	@Override
	public Singer update(Singer singer) {
		return this.singerRepository.update(singer);
	}

	@Transactional
	@Override
	public void delete(Singer singer) {
		this.singerRepository.delete(singer);
	}

	@Override
	@Transactional
	public void unlinkInstrument(Long singerId, String instrumentId) {
		Singer s = this.findById(singerId);
		Instrument i = this.instrumentRepository.findById(instrumentId);
		s.getInstruments().remove(i);		
	}
	
	@Autowired
	public void setSingerRepository(SingerDao singerRepository) {
		this.singerRepository = singerRepository;
	}
	
	@Autowired
	public void setInstrumentRepository(InstrumentDao instrumentRepository) {
		this.instrumentRepository = instrumentRepository;
	}

	@Override
	public void delete(Long singerId) {
		Singer s = this.findById(singerId);
		this.singerRepository.delete(s);
	}


}
